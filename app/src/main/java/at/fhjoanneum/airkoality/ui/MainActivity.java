package at.fhjoanneum.airkoality.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;


import at.fhjoanneum.airkoality.R;
import at.fhjoanneum.airkoality.ui.fragment.LocationListFragment;
import at.fhjoanneum.airkoality.ui.fragment.MapFragment;

public class MainActivity extends AppCompatActivity {

    private LocationListFragment locationListFragment;
    private MapFragment mapFragment;

    private static final String FRAGMENT_LOCATION = "FragmentLocation";
    private static final String FRAGMENT_MAP = "FragmentMap";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_activity);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bnvMain);

        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_location:
                    switchToFragment(FRAGMENT_LOCATION);
                    break;
                case R.id.action_map:
                    switchToFragment(FRAGMENT_MAP);
                    break;
                default:
                    break;
            }
//            Listener will unbedingt einen return Wert haben
            return true;
        });

        locationListFragment = new LocationListFragment();
        mapFragment = new MapFragment();

        FragmentManager fm = getSupportFragmentManager();

        FragmentTransaction transaction = fm.beginTransaction();
        transaction.add(R.id.flFragmentContainer, locationListFragment);
        transaction.add(R.id.flFragmentContainer, mapFragment);
        transaction.hide(locationListFragment);
        transaction.hide(mapFragment);
        transaction.commit();

        switchToFragment(FRAGMENT_LOCATION);
    }

    private void switchToFragment(String fragmentName) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (fragmentName) {
            case FRAGMENT_LOCATION:
                transaction.hide(mapFragment);
                transaction.show(locationListFragment);
                break;
            case FRAGMENT_MAP:
                transaction.hide(locationListFragment);
                transaction.show(mapFragment);
                break;
            default:
                break;
        }
        transaction.commit();
    }
}
