package at.fhjoanneum.airkoality.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import at.fhjoanneum.airkoality.R;
import at.fhjoanneum.airkoality.model.Location;

public class LocationListAdapter extends RecyclerView.Adapter<LocationListAdapter.LocationViewHolder> {

    private List<Location> locations;
    private LocationListItemClickListener listener;

    public LocationListAdapter(List<Location> locations, LocationListItemClickListener listener) {
        this.locations = locations;
        this.listener = listener;
    }

    @NonNull
    @Override
    public LocationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.location_list_item, parent, false);

        return new LocationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LocationViewHolder holder, int position) {
        holder.bindItem(locations.get(position));
    }

    @Override
    public int getItemCount() {
        return locations.size();
    }

    class LocationViewHolder extends RecyclerView.ViewHolder {
        private View cvRootView;
        private TextView tvLocation;
        private TextView tvCity;
        private TextView tvCountry;

        public LocationViewHolder(@NonNull View itemView) {
            super(itemView);

            cvRootView = itemView.findViewById(R.id.cvRootView);
            tvLocation = itemView.findViewById(R.id.tvLocation);
            tvCity = itemView.findViewById(R.id.tvCity);
            tvCountry = itemView.findViewById(R.id.tvCountry);
        }

        public void bindItem(Location location) {
            tvLocation.setText(location.getLocation());
            tvCity.setText(location.getCity());
            tvCountry.setText(location.getCountry());

            cvRootView.setOnClickListener(view -> {
                listener.onItemClicked(location);
            });
        }
    }

    public interface LocationListItemClickListener {
        void onItemClicked(Location location);
    }
}
