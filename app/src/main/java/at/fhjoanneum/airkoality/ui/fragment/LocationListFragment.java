package at.fhjoanneum.airkoality.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import at.fhjoanneum.airkoality.R;
import at.fhjoanneum.airkoality.model.Location;
import at.fhjoanneum.airkoality.ui.adapter.LocationListAdapter;

public class LocationListFragment extends Fragment implements LocationListAdapter.LocationListItemClickListener {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_location, container, false);

        RecyclerView rvLocations = view.findViewById(R.id.rvLocations);
        rvLocations.setLayoutManager(new LinearLayoutManager(getContext()));

        List<Location> locations = new ArrayList<>();

        for (int i = 1; i < 100; i++) {
            locations.add(new Location("Location " + i, "Graz", "Österreich"));
        }

        LocationListAdapter locationListAdapter = new LocationListAdapter(locations, this);

        rvLocations.setAdapter(locationListAdapter);

        return view;
    }

    @Override
    public void onItemClicked(Location location) {
        Toast.makeText(getContext(), location.getLocation(), Toast.LENGTH_SHORT).show();
    }
}
